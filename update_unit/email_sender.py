import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# EmailClient class which contain authentication information and other information
class EmailClient(object):

    # constructor of EmailClient
    def __init__(self, my_email, password, recipient_email, server, port):
        self.my_email = my_email
        self.password = password
        self.recipient_email = recipient_email
        self.server = server
        self.port = port

    # method sends updates: two tables as attachment
    def send_message(self, subject, trades_table, portfolio_table):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = self.my_email
        msg['To'] = self.recipient_email

        # attaching Portfolio table
        msg.attach(MIMEText("Portfolio", 'plain'))
        if portfolio_table != None:
            msg.attach(MIMEText(portfolio_table, 'html'))

        # attaching Trades table
        msg.attach(MIMEText("Trades", 'plain'))
        if trades_table != None:
            msg.attach(MIMEText(trades_table, 'html'))

        # authenticating in mail server
        #############################################
        session = smtplib.SMTP(self.server, self.port)
        session.ehlo()
        session.starttls()
        session.ehlo
        session.login(self.my_email, self.password)
        #############################################

        session.sendmail(self.my_email, self.recipient_email, msg.as_string())
        session.quit()

