#https://www.twilio.com/docs/libraries/python#installation
from twilio.rest import TwilioRestClient

# getTwilioClient class creates TwilioRestClient
# and allows to send message from Twilio phone number
class getTwilioClient(object):

    # constructor of getTwilioClient class
    def __init__(self, account_sid, auth_token, sender_num, recipient_num):
        self.account_sid = account_sid
        self.auth_token = auth_token
        self.sender_num = sender_num
        self.recipient_num = recipient_num
        self.rest_client = TwilioRestClient(account_sid, auth_token)

    # method send short 'message_text'
    # from 'sender_num' to 'recipient_num'
    def send_sms(self, message_text):
        message = self.rest_client.messages.create(to=self.recipient_num,
                                                   from_=self.sender_num,
                                                   body=message_text)