#########################################################################
                                Easy Scraper
#########################################################################

Program was wrote with python3
Used libraries: requests, BeautifulSoup 4, twilio, smtplib, email, etc
Author: Alexander Petrick

Program visit https://www.wikifolio.com/de/de/wikifolio/platintrader-1000-leidenschaft every two minutes
and check if HTML table Trades has new items. If there is a new item, script sends Email with Trades and Portfolio tables,
also sends SMS only with new items from Trades table.

SMS example:
################

Sent from your Twilio trial account - New updates! 2
##########
stock_name:  KUKA AG | DE0006204407;
profit:  N/A;
buy_sell:  Quote Kauf;
status:  Ausgeführt | 15.07.2016 16:24;
quantity:  3.000;
amount_to_invest:  1251000.000;
price:  107,338;
weight:  16,68 %;
##########
stock_name:  KUKA AG | DE0006204407;
profit:  N/A;
buy_sell:  Quote Kauf;
status:  Ausgeführt | 15.07.2016 16:24;
quantity:  2.000;
amount_to_invest:  834000.000;
price:  107,338;
weight:  11,12 %;
##########

HOW TO INSTALL
#################

1. On mashine must be installed python3 and pip

2. Then run:

# pip install -r requirements.txt

It must install all required libraries.

HOW TO RUN SCRIPT
##################
1. First enter your configs in ./config/ folder:

    * email_config.json
    * twilio_config.json

2. Enter command in the terminal:

$ python start.py
