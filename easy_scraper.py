import requests
import re
from bs4 import BeautifulSoup

headers = {'User-agent': 'Mozilla/5.0'}
buffered_items = None   # buffered Trades table`s items
session = None
cookies = None
table_items_size = 0    # expected total item of Trades table scraped before fetching whole table
secure_token = None # secure token, must be given into cookies
request_id = None   # id used in request's method

# base function of scraping
# returns Trades table, updates of Trades table and Portfolio table
# as tuple (trades_table, updates, portfolio_table)
def scrap_html(base_url,table_url):
    global cookies, session,table_items_size,secure_token, request_id

    if session == None:
        session = requests.Session()
        res = session.get(base_url, headers=headers )

        set_cookies = res.headers['Set-Cookie'].split(';')
        cookies = session.cookies.get_dict()
        for param in set_cookies:
            req_verif_token = re.search(r'__RequestVerificationToken=(.*)', param)
            if req_verif_token != None:
                req_verif_token = req_verif_token.group(1)
                break

        cookies['__RequestVerificationToken'] = req_verif_token
    else:
        res = session.get(base_url, headers=headers, cookies=cookies)

    page_data = res.text
    soup = BeautifulSoup(page_data, 'html.parser')

    # finding main div block where Trades table locates
    trades_content = soup.find("div", {"id": "trades-paged-content-list", "class": "wikifolio-trades-view "})

    # finding values used in security context
    secure_token = {"__RequestVerificationToken": soup.find("input", {"name": "__RequestVerificationToken"}).get("value")}
    request_id = soup.find("input", {"id": "wikifolio"}).get("value")


    table_items_size = get_table_items_count(table_url,trades_content.find("li", {"class": "page-number PagedList-skipToLast"}).a.get('href'))
    print("EXPECTED ITEM COUNT: "+str(table_items_size))

    # build a new table link to get all items at once
    table_url = table_url % (request_id, 1, table_items_size)

    trades_table, updates = scrap_trades_tables(table_url)
    # if updates > 0:
    portfolio_table = scrap_portfolio_table(base_url)
    return (trades_table, updates, portfolio_table)

# function gets table 'Portfolio' in html format
def scrap_portfolio_table(base_url):
    res = session.get(base_url, headers=headers, cookies=cookies)
    soup = BeautifulSoup(res.text, 'html.parser')
    div_colaps = soup.find("div", {"id": "collapsibleTickData"})
    trade_table = str(div_colaps.table)
    return trade_table

# function downloads entire Trades table, checks new items with buffer
# and returns Trades html table as first parameter
# and returns only new unique items of Trades
def scrap_trades_tables(url):
    global buffered_items,cookies,current_proxy,table_items_size

    res = session.get(url, headers=headers, cookies=cookies, data=secure_token)
    page_data = res.text
    table_data = BeautifulSoup(page_data, 'html.parser')
    items = list(get_item_from_table(table_data.table))
    item_updetes = []

    if buffered_items == None:  # if buffer is empty, then create it
        # Initializing buffer
        buffered_items = []
        for item in items:
            buffered_items.append(item)
            item_updetes.append(item)
        print('BUFF_ITEMS: ', len(buffered_items))
    else:                       #otherwise, check for new items in 'Trades' table
        new_items = []
        for item in items:
            new_items.append(item)

            if not contains(item, buffered_items):
                item_updetes.append(item)   # list of new items of 'Trades' table

        print('NEW UPDATES: ', len(item_updetes))
        print('BUFF_ITEMS: ', len(new_items))
        assert table_items_size == len(new_items)
        buffered_items = new_items

    return (page_data, item_updetes)

# generator which returns parsed and packed in dictionaries
# items of 'Trade' table
def get_item_from_table(table):
    for tr in table.findAll("tr", {"class": "parent-order"}):
        item = {}
        tds = tr.findAll("td")
        for td_index in range(len(tds)):
            if td_index == 0:
                try:
                    main_div = tds[td_index].find("div", {"class": "double-line-first"})
                    item['stock_name'] = " ".join((main_div.a.span.string + ' | ' \
                                                   + main_div.find("div", {"class": "isin"}).string).split())
                except:
                    item['stock_name'] = "N/A"
            elif td_index == 1:
                try:
                    item['buy_sell'] = " ".join(tds[td_index].string.split())
                except:
                    item['buy_sell'] = "N/A"
            elif td_index == 2:
                try:
                    td_block = tds[td_index]
                    item['status'] = " ".join((td_block.find("div", {"class": "text-bold"}).string + ' | ' \
                                               + td_block.find("div", {"class": "smalldatetime"}).string).split())
                except:
                    item['status'] = "N/A"
            elif td_index == 3:
                try:
                    item['price'] = " ".join(tds[td_index].string.split())
                except:
                    item['price'] = "N/A"
            elif td_index == 4:
                try:
                    item['quantity'] = " ".join(tds[td_index].string.split())
                except:
                    item['quantity'] = "N/A"
            elif td_index == 5:
                try:
                    item['weight'] = weight = " ".join(tds[td_index].span.string.split())
                    try:
                        item['amount_to_invest'] = "%.3f" % (
                        75000 * float(re.sub(r',', ".", re.sub(r'[^0-9,]', "", weight))))
                    except ValueError:
                        item['amount_to_invest'] = "N/A"
                except:
                    item['amount_to_invest'] = "N/A"
            elif td_index == 6:
                try:
                    try:
                        item['profit'] = " ".join(tds[td_index].span.string.split())
                    except AttributeError:
                        item['profit'] = " ".join(tds[td_index].div.string.split())
                except:
                    item['profit'] = "N/A"
        yield item

# function gets expected value of all items in 'Trades' table
def get_table_items_count(table_url, last_page):
    global session, cookies, request_id

    pages_count = int(re.sub(r'.*#t',"", last_page))
    res = session.get(table_url % (request_id, pages_count, 5),
                      headers=headers,
                      cookies=cookies,
                      data=secure_token)
    page_data = res.text
    table_data = BeautifulSoup(page_data, 'html.parser')
    table = table_data.table    # Trades table returned by GET request
    items_on_last_page = len(table.findAll("tr", {"class": "parent-order"}))
    return ((pages_count-1)*5 + items_on_last_page)

# function checks is dictionary contains in
# list of dictionaries
def contains(item, buff_items):
    for buff_item in buff_items:
        if equals_dict(item, buff_item):
            return True
    return False

# function checks whether the two dictionaries equal
def equals_dict(d1, d2):
    both = d1.keys() & d2.keys()
    diff = {k:(d1[k], d2[k]) for k in both if d1[k] != d2[k]}
    if len(diff) == 0:
        return True
    else:
        return False
