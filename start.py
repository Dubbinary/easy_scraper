import os
import time
import json
import easy_scraper
import requests
from update_unit import twilio_api, email_sender

configs_locate = "./config/"
base_url = None
table_url = None
email_client = None
twilio_client = None
timeout = None
is_buffered = False

# function that loads configuration from a JSON files
# in a 'config/' folder and setup them
def load_configuration():
    global base_url, table_url, email_client, twilio_client, timeout
    if twilio_client == None and twilio_client == None:
        print("LOADING CONFIGURES....")
        config = config_files_loader()      # loading configures in the dictionary 'config'
        base_url = config['base_url']       # loading base URL which use to scrape main information
        table_url = config['table_url']     # loading table URL which use to scrape 'Trades' table
        timeout = int(config['timeout'])    # loading timeout in seconds

        account_sid = config['account_sid']     # loading Twilio's account SID
        auth_token = config['auth_token']       # loading Twilio's authentication token
        sender_num = config['sender_num']       # loading Twilio's number for sending SMS
        recipient_num = config['recipient_num'] # loading recipient number for receiving SMS
        twilio_client = twilio_api.getTwilioClient(account_sid, auth_token,sender_num,recipient_num)

        sender_email = config['sender_email']
        password = config['password']
        recipient_email = config['recipient_email']
        server = config['server']
        port = config['port']
        email_client = email_sender.EmailClient(sender_email,password,recipient_email,server,port)
    pass

# function loads configures from files
# in a 'config/' folder into one dictionary
def config_files_loader():
    global configs_locate
    config = {}
    for file in os.listdir("./config"):
        if file.endswith('.json'):
            with open(configs_locate+file, 'r') as rfile:
                config.update(json.load(rfile))
    return config


# function send updates using twilio and mail clients
def send_an_update(text, trade_table=None, updates=None, portfolio_table=None):
    global email_client, twilio_client
    print('[INFO]: Sending updates...')
    if trade_table != None and updates != None and portfolio_table != None:
        twilio_client.send_sms(text+human_view_dict(updates))
        email_client.send_message(text, trade_table, portfolio_table)
    else:
        twilio_client.send_sms(text)
        email_client.send_message(text, None, None)

# function prepares dictionary items to be sent via SMS
def human_view_dict(dicts):
    string = "##########\n"
    for item in dicts:
        for key in item.keys():
            string += key+": \t"
            string += item[key]+"; \n"
        string+="##########\n"
    return string

# function save html data in a file by given path
def save_html(path, data):
    with open(path+time.strftime("%d_%m_%Y_%H:%M:%S")+".html", 'wt') as file:
        file.write(data)

# the main function of the script, there controls all
# functionality: scraping, sending notification, and doing backups
def perform():
    global timeout, is_buffered
    load_configuration()
    while True:
        try:
            # eutil = export_utility.XmlSaver('stock_name', 'buy_sell', 'status', 'price', 'quantity', 'weight', 'amount_to_invest', 'profit')
            trade_table, updates, portfolio_table = easy_scraper.scrap_html(base_url, table_url)
        except requests.exceptions.ConnectionError:
            send_an_update("Connection aborted")
        # # uncomment to do backup of tables locally
        # save_html('./html/trade_table', trade_table)
        # save_html('./html/portfolio_table', portfolio_table)

        # checking if new items are,
        # if yes, then triggers send_an_update function
        if len(updates) > 0 and is_buffered:
            send_an_update("New updates! "+str(len(updates))+"\n", trade_table, updates, portfolio_table)

        is_buffered =True
        print("GO SLEEP")
        time.sleep(timeout)
        print("WAKE UP")


if __name__ == '__main__':
    try:
        perform()
        print("Exit program")
    except:
        send_an_update("Error was occurred!\n Exit from program.")